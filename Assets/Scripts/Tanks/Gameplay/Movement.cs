﻿using UnityEngine;


namespace Tanks.Gameplay
{
    public class Movement : MonoBehaviour
    {
        [SerializeField]
        private float speed;

        private Rigidbody2D rb;
        private RectTransform rt;

        private Vector2 velocity;
        public Rotation thisRotation { get; private set; }


        private void Awake ()
        {
            rb = GetComponent<Rigidbody2D>();
            rt = GetComponent<RectTransform>();
        }

        private void FixedUpdate()
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                ChangeRotation(Rotation.Up);
                Vector2 velocity = new Vector2(0,speed);
                rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                ChangeRotation(Rotation.Down);
                Vector2 velocity = new Vector2(0, -speed);
                rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                ChangeRotation(Rotation.Left);
                Vector2 velocity = new Vector2(-speed, 0);
                rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                ChangeRotation(Rotation.Right);
                Vector2 velocity = new Vector2(speed, 0);
                rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
            }
        }

        private void ChangeRotation (Rotation rot)
        {
            if(rot != thisRotation)
            {
                thisRotation = rot;
                switch (rot)
                {
                    case (Rotation.Up):
                        rt.localEulerAngles = Vector3.zero;
                        break;
                    case (Rotation.Down):
                        rt.localEulerAngles = new Vector3(0,0,180);
                        break;
                    case (Rotation.Left):
                        rt.localEulerAngles = new Vector3(0, 0, 90);
                        break;
                    case (Rotation.Right):
                        rt.localEulerAngles = new Vector3(0, 0, -90);
                        break;
                }
            }
        }
    }
}