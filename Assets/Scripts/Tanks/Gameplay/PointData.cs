﻿using UnityEngine;

namespace Tanks.Gameplay
{
    public class PointData
    {
        public Vector2Int coordinates { get; private set; }
        public Transform pointTransform { get; private set; }
        public float distanceToBase { get; private set; }
        public int type { get; private set; }
        public Rotation rotation { get; private set; }


        public PointData (Vector2Int _coordinates, int _type, Rotation _rotation)
        {
            coordinates = _coordinates;
            type = _type;
            rotation = _rotation;
            SetParams();
        }

        private void SetParams ()
        {
            pointTransform = SpawnController.Instance.GetPlatePosition(coordinates.x, coordinates.y);
            distanceToBase = Vector2.Distance(pointTransform.position, SpawnController.Instance.GetBasePosition());
        }
    }
}