﻿namespace Tanks.Gameplay
{
    public enum BonusType
    {
        Drive,
        Gun,
        Body,
        Stun,
        MegaBase,
        KillThemAll,
        Insane
    }
}