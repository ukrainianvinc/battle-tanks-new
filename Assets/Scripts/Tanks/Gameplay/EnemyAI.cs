﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Tanks.Gameplay
{
    public class EnemyAI : MonoBehaviour
    {
        [SerializeField]
        private int speed;
        [SerializeField]
        private Vector2Int startPos;
        [SerializeField]
        private Bullet bullet;
        [SerializeField]
        private RectTransform bulletSpawn;
        [SerializeField]
        private float shootDelay;
        [SerializeField]
        private float hp;
        [SerializeField]
        private Bonus[] bonuses;
        [SerializeField]
        private Image tankBody;

        private RectTransform rt;
        private int[,] fieldMatrix;
        private EnemyMovement move;

        private Rotation thisRotation;
        private PointData lastPos;
        private PointData thisPos;
        private bool mayShoot = true;
        private EnemySpawnPoint enemySpawnPoint;
        bool isBonusTank;


        private void Awake ()
        {
            fieldMatrix = SpawnController.Instance.GetMatrix();
            rt = GetComponent<RectTransform>();
            move = GetComponent<EnemyMovement>();
        }

        public void StartPlay (EnemySpawnPoint point, bool bonusTank = false)
        {
            enemySpawnPoint = point;
            startPos = point.coordinates;
            thisPos = new PointData(new Vector2Int(startPos.x, startPos.y), fieldMatrix[startPos.x, startPos.y], Rotation.Up);
            if(bonusTank)
            {
                Debug.Log("Bonus");
                isBonusTank = true;
                StartCoroutine(BonusLight());
            }
            SetNextPlate();
        }

        public void SetNextPlate ()
        {
            List<PointData> data = new List<PointData>();
            AddDots(data);
            PointData bestPlate;
            if (lastPos == null)
            {
                bestPlate = thisPos;
            }
            else
            {
                bestPlate = lastPos;
            }
            bool haveEmptyPlate = false;
            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].type == 0 && data[i].distanceToBase < bestPlate.distanceToBase)
                {
                    haveEmptyPlate = true;
                    bestPlate = data[i];
                }
            }
            if (!haveEmptyPlate)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    if (data[i].type == 1 && data[i].distanceToBase < bestPlate.distanceToBase)
                    {
                        haveEmptyPlate = true;
                        bestPlate = data[i];
                    }
                }
            }
            MoveToNextPlate(bestPlate);
        }

        private void MoveToNextPlate (PointData plate)
        {
            lastPos = thisPos;
            thisPos = plate;
            RotateEnemy(plate.rotation);
            move.StartMoveToPoint(plate.pointTransform.position);
        }

        private void OnCollisionStay2D (Collision2D col)
        {
            if (col.gameObject.tag == "Block")
            {
                Shoot();
            }
        }

        private void RotateEnemy (Rotation rot)
        {
            thisRotation = rot;
            switch (rot)
            {
                case (Rotation.Up):
                    rt.localEulerAngles = Vector3.zero;
                    break;
                case (Rotation.Down):
                    rt.localEulerAngles = new Vector3(0, 0, 180);
                    break;
                case (Rotation.Left):
                    rt.localEulerAngles = new Vector3(0, 0, 90);
                    break;
                case (Rotation.Right):
                    rt.localEulerAngles = new Vector3(0, 0, -90);
                    break;
            }
        }

        private void AddDots (List<PointData> data)
        {
            if (thisPos.coordinates.x > 0)
            {
                PointData dot = new PointData(new Vector2Int(thisPos.coordinates.x - 1, thisPos.coordinates.y), 
                    fieldMatrix[thisPos.coordinates.x - 1, thisPos.coordinates.y],Rotation.Left);
                data.Add(dot);
            }
            if (thisPos.coordinates.x < 12)
            {
                PointData dot = new PointData(new Vector2Int(thisPos.coordinates.x + 1, thisPos.coordinates.y), 
                    fieldMatrix[thisPos.coordinates.x + 1, thisPos.coordinates.y], Rotation.Right);
                data.Add(dot);
            }
            if (thisPos.coordinates.y > 0)
            {
                PointData dot = new PointData(new Vector2Int(thisPos.coordinates.x, thisPos.coordinates.y - 1), 
                    fieldMatrix[thisPos.coordinates.x, thisPos.coordinates.y - 1], Rotation.Up);
                data.Add(dot);
            }
            if (thisPos.coordinates.y < 12)
            {
                PointData dot = new PointData(new Vector2Int(thisPos.coordinates.x, thisPos.coordinates.y + 1), 
                    fieldMatrix[thisPos.coordinates.x, thisPos.coordinates.y + 1], Rotation.Down);
                data.Add(dot);
            }
        }

        private void Update ()
        {
            CheckPlayerOrBase();
        }

        private void CheckPlayerOrBase ()
        {
            RaycastHit2D hit = new RaycastHit2D();
            switch (thisRotation)
            {
                case (Rotation.Up):
                    hit = Physics2D.Raycast(bulletSpawn.position, Vector2.up,10);
                    break;
                case (Rotation.Down):
                    hit = Physics2D.Raycast(bulletSpawn.position, Vector2.down, 10);
                    break;
                case (Rotation.Left):
                    hit = Physics2D.Raycast(bulletSpawn.position, Vector2.left, 10);
                    break;
                case (Rotation.Right):
                    hit = Physics2D.Raycast(bulletSpawn.position, Vector2.right, 10);
                    break;
            }
            if (hit.collider != null)
            {
                if(hit.collider.gameObject.tag != "Enemy" && (hit.collider.gameObject.tag == "Player" 
                    || hit.collider.gameObject.tag == "Base"))
                {
                    Shoot();
                }
            }
        }

        private void Shoot ()
        {
            if(mayShoot)
            {
                StartCoroutine(ShootDelay());
                InnerShoot();
            }
        }

        private void InnerShoot ()
        {
            Bullet temp = Instantiate(bullet);
            temp.transform.SetParent(transform.root, false);
            temp.SetParams(thisRotation, bulletSpawn.position);
        }

        private IEnumerator ShootDelay()
        {
            mayShoot = false;
            yield return new WaitForSeconds(shootDelay);
            mayShoot = true;
        }

        public void Damage ()
        {
            hp--;
            if(hp <= 0)
            {
                Death();
            }
        }

        private void Death ()
        {
            SpawnController.Instance.SpawnNewEnemy(enemySpawnPoint);
            GameController.Instance.AddKilled();
            if(isBonusTank)
            {
                SpawnBonus();
            }
            Destroy(gameObject);
        }

        private void SpawnBonus ()
        {
            Transform temp = Instantiate(bonuses[Random.Range(0, bonuses.Length)]).transform;
            temp.SetParent(transform.root,false);
            temp.position = transform.position;
        }

        private IEnumerator BonusLight ()
        {
            Color main = tankBody.color;
            while(true)
            {
                tankBody.color = Color.white;
                yield return new WaitForSeconds(0.2f);
                tankBody.color = main;
                yield return new WaitForSeconds(0.2f);
            }
        }
    }
}