﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tanks.Gameplay
{
    public class EnemySpawnPoint : MonoBehaviour
    {
        public Vector2Int coordinates;
    }
}