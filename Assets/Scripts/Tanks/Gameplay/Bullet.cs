﻿using UnityEngine;

namespace Tanks.Gameplay
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField]
        private int speed;

        private Rigidbody2D rb;
        private RectTransform rt;
        private Player player;

        private Rotation thisRotation = Rotation.Up;
        private bool destroyed;


        private void Awake ()
        {
            rb = GetComponent<Rigidbody2D>();
            rt = GetComponent<RectTransform>();
        }

        public void SetParams(Rotation rot, Vector3 spawnPos)
        {
            transform.position = spawnPos;
            thisRotation = rot;
            switch (rot)
            {
                case (Rotation.Up):
                    rt.localEulerAngles = Vector3.zero;
                    break;
                case (Rotation.Down):
                    rt.localEulerAngles = new Vector3(0, 0, 180);
                    break;
                case (Rotation.Left):
                    rt.localEulerAngles = new Vector3(0, 0, 90);
                    break;
                case (Rotation.Right):
                    rt.localEulerAngles = new Vector3(0, 0, -90);
                    break;
            }
        }

        private void FixedUpdate ()
        {
            Vector2 velocity = Vector2.zero; ;
            switch (thisRotation)
            {
                case (Rotation.Up):
                    velocity = new Vector2(0, speed);
                    break;
                case (Rotation.Down):
                    velocity = new Vector2(0, -speed);
                    break;
                case (Rotation.Left):
                    velocity = new Vector2(-speed, 0);
                    break;
                case (Rotation.Right):
                    velocity = new Vector2(speed, 0);
                    break;
            }
            rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
        }

        private void OnTriggerEnter2D (Collider2D col)
        {
            if (!destroyed)
            {
                if (col.tag == "Block")
                {
                    destroyed = true;
                    Player.Instance.destroyedBullet = true;
                    Destroy(col.gameObject);
                }
                else if (col.tag == "Border")
                {
                    destroyed = true;
                    Player.Instance.destroyedBullet = true;
                }
                else if(col.tag == "Enemy")
                {
                    destroyed = true;
                    col.GetComponent<EnemyAI>().Damage();
                }
                else if(col.tag == "Player")
                {
                    destroyed = true;
                    col.GetComponent<Player>().Damage();
                }
                else if (col.tag == "Base")
                {
                    GameController.Instance.GameOver();
                }
                Destroy(gameObject);
            }
        }
    }
}