﻿namespace Tanks.Gameplay
{
    public enum Rotation
    {
        Up,
        Down,
        Left,
        Right
    }
}