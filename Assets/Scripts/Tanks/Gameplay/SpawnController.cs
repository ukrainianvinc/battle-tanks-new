﻿using UnityEngine;

namespace Tanks.Gameplay
{
    public class SpawnController : MonoBehaviour
    {
        [SerializeField]
        private Transform grid;
        [SerializeField]
        private Transform enemiesObj;
        [SerializeField]
        private Transform[] items;
        [SerializeField]
        private Transform basePosition;
        [SerializeField]
        private EnemySpawnPoint[] enemySpawnsPoints;
        [SerializeField]
        private EnemyAI[] enemies;

        private Vector2Int[] basePos = {new Vector2Int(11,5), new Vector2Int(11, 6), new Vector2Int(11, 7),
            new Vector2Int(12, 5), new Vector2Int(12, 6), new Vector2Int(12, 7) };
        public int[,] fieldMatrix { get; private set; }
        private const int gridHeight = 13;
        private const int gridWidth = 13;

        public static SpawnController Instance;


        private void Awake ()
        {
            Instance = this;
            fieldMatrix = new int[13, 13];
        }

        public void SpawnChestDesk()
        {
            for (int i = 0; i < gridHeight; i++)
            {
                for (int j = 0; j < gridWidth; j++)
                {
                    if ((i+j) % 2 != 0)
                    {
                        fieldMatrix[i, j] = 1;
                        CreateItem(items[1], new Vector2Int(i, j));
                    }
                    else
                    {
                        fieldMatrix[i, j] = 0;
                        CreateItem(items[0], new Vector2Int(i, j));
                    }
                }
            }

        }

        private void CreateItem(Transform item, Vector2Int pos)
        {
            for (int i = 0; i < basePos.Length; i++)
            {
                if(pos == basePos[i])
                {
                    Transform empty = Instantiate(items[0]);
                    empty.SetParent(grid, false);
                    return;
                }
            }
            Transform temp = Instantiate(item);
            temp.SetParent(grid, false);
        }

        public void SpawnFirstEnemies ()
        {
            for (int i = 0; i < enemySpawnsPoints.Length; i++)
            {
                EnemyAI temp = Instantiate(enemies[Random.Range(0,enemies.Length)]);
                temp.transform.SetParent(enemiesObj, false);
                temp.transform.position = enemySpawnsPoints[i].transform.position;
                temp.StartPlay(enemySpawnsPoints[i]);
            }
        }

        public void SpawnNewEnemy (EnemySpawnPoint point)
        {
            EnemyAI temp = Instantiate(enemies[Random.Range(0, enemies.Length)]);
            temp.transform.SetParent(enemiesObj, false);
            temp.transform.position = point.transform.position;
            if (GameController.Instance.killed == 1 || GameController.Instance.killed == 5 || GameController.Instance.killed == 12)
            {
                temp.StartPlay(point,true);
            }
            else
            {
                temp.StartPlay(point);
            }
        }

        public Transform GetPlatePosition(int x, int y)
        {
            return grid.GetChild(x + (y*13));
        }

        public Vector2 GetBasePosition()
        {
            return basePosition.position;
        }

        public int[,] GetMatrix ()
        {
            return fieldMatrix;
        }
    }
}