﻿namespace Tanks.Gameplay
{
    public enum Target
    {
        Block,
        Enemy,
        Player,
    }
}