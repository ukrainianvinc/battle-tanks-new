﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Tanks.Gameplay
{
    public class GameController : MonoBehaviour
    {
        private int lvl;
        public int killed { get; private set; }


        public static GameController Instance;


        private void Awake ()
        {
            Instance = this;
            killed = 0;
        }

        private void Start ()
        {
            lvl = PlayerPrefs.GetInt(Keys.lastLevel) + 1;
            switch(lvl)
            {
                case (1):
                    SpawnController.Instance.SpawnChestDesk();
                    break;
                case (2):
                    SpawnController.Instance.SpawnChestDesk();
                    break;
                case (3):
                    SpawnController.Instance.SpawnChestDesk();
                    break;
            }
            StartCoroutine(SpawnAfterDelay());
        }

        public void AddKilled ()
        {
            killed++;
            if(killed >= 20)
            {
                NextLevel();
            }
        }

        private void NextLevel ()
        {
            if (lvl < 3)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
            else
            {
                SceneManager.LoadScene("Menu");
            }
        }

        public void GameOver ()
        {
            SceneManager.LoadScene("Menu");
        }

        IEnumerator SpawnAfterDelay()
        {
            yield return new WaitForSeconds(0.1f);
            SpawnController.Instance.SpawnFirstEnemies();
        }
    }
}