﻿using UnityEngine;
using System.Collections;

namespace Tanks.Gameplay
{
    public class Player : MonoBehaviour
    {
        [SerializeField]
        private Bullet bullet;
        [SerializeField]
        private RectTransform bulletSpawn;
        [SerializeField]
        private float delay;
        [SerializeField]
        private Transform spawnPos;

        private Movement move;

        private bool mayShoot = true;
        [HideInInspector]
        public bool destroyedBullet = true;
        public bool automatic { get; private set; }
        private int hp = 1;
        private int gun = 1;
        private int body = 1;
        private int drive = 1;

        public static Player Instance;


        private void Awake ()
        {
            Instance = this;
            move = GetComponent<Movement>();
        }

        private void Update ()
        {
            if (Input.GetKey(KeyCode.Space))
            {
                Shoot();
            }
        }

        public void Shoot()
        {
            if (mayShoot && automatic)
            {
                InnerShoot();
            }
            else if (mayShoot && destroyedBullet)
            {
                InnerShoot();
            }
        }

        public void Damage ()
        {
            hp--;
            if(hp >= 0)
            {
                Restart();
            }
        }

        private void Restart ()
        {
            gun = 1;
            body = 1;
            drive = 1;
            transform.position = spawnPos.position;
            SetEquip();
        }

        public void Upgrade (BonusType bonus)
        {
            switch (bonus)
            {
                case BonusType.Gun:
                    gun++;
                    break;
                case BonusType.Body:
                    body++;
                    break;
                case BonusType.Drive:
                    drive++;
                    break;
            }
            SetEquip();
        }

        private void SetEquip ()
        {
            hp = body;
            delay = delay / gun;
            if(gun == 3)
            {
                automatic = true;
            }
            else
            {
                automatic = false;
            }
            hp = body; 
        }

        private void InnerShoot ()
        {
            StartCoroutine(ShootDelay());
            Bullet temp = Instantiate(bullet);
            temp.transform.SetParent(transform.root,false);
            temp.SetParams(move.thisRotation, bulletSpawn.position);
            destroyedBullet = false;
        }
        
        private IEnumerator ShootDelay ()
        {
            mayShoot = false;
            yield return new WaitForSeconds(delay);
            mayShoot = true;
        }
    }
}