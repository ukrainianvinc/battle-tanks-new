﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tanks.Gameplay
{
    public class EnemyMovement : MonoBehaviour
    {
        [SerializeField]
        private float speed;

        private EnemyAI ai;

        private Vector2 targetPos;


        private void Awake ()
        {
            ai = GetComponent<EnemyAI>();
        }

        public void StartMoveToPoint (Vector2 _targetPos)
        {
            enabled = true;
            targetPos = _targetPos;
        }

        private void FixedUpdate ()
        {
            if ((Vector2)transform.position != targetPos)
            {
                transform.position = Vector2.MoveTowards(transform.position, targetPos, speed * Time.fixedDeltaTime);
            }
            else
            {
                enabled = false;
                ai.SetNextPlate();
            }
        }
    }
}