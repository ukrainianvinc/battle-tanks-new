﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tanks.Gameplay
{
    public class Bonus : MonoBehaviour
    {
        [SerializeField]
        BonusType bonusType;


        private void OnTriggerEnter2D (Collider2D col)
        {
            if(col.tag == "Player")
            {
                switch(bonusType)
                {
                    case BonusType.Gun:
                        Player.Instance.Upgrade(bonusType);
                        break;
                    case BonusType.Body:
                        Player.Instance.Upgrade(bonusType);
                        break;
                    case BonusType.Drive:
                        Player.Instance.Upgrade(bonusType);
                        break;
                }
                Destroy(gameObject);
            }
        }
    }
}