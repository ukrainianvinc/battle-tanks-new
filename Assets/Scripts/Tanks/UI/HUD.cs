﻿using UnityEngine.UI;
using UnityEngine;
using Tanks.Gameplay;

namespace Tanks.UI
{
    public class HUD : MonoBehaviour
    {
        [SerializeField]
        private Text lvl;
        [SerializeField]
        private Text score;
        [SerializeField]
        private Text killed;
        [SerializeField]
        private Text hp;
        [SerializeField]
        private Text drive;
        [SerializeField]
        private Text gun;
        [SerializeField]
        private Text body;


        public void UpdateParams ()
        {

        }
    }
}