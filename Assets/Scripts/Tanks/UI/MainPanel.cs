﻿using UnityEngine.SceneManagement;
using UnityEngine;

namespace Tanks.UI
{
    public class MainPanel : MonoBehaviour
    {
        public void LoadScene (string sceneName)
        {
            PlayerPrefs.SetInt(Keys.lastLevel,0);
            SceneManager.LoadScene(sceneName);
        }
    }
}